$(document).ready(function() {
    // Stripe.setPublishableKey('pk_test_Ge2L6a4JOHun7gOry1fbdyQZ');	// Test Key
	Stripe.setPublishableKey('pk_live_vpIZA5Qagy5tAs8zS9H7Ko9R');	
	$("input:radio[name=optionsRadios]").change(function() {
		var price;
		if ($("input:radio[value=0]").prop("checked")) {
			price = "69";
		} else {
			price = "29";	
		}
		$("span.price").html("$" + price +".00</p>");	
			
	});

	// Billing Validation 
	$('input#number').payment('formatCardNumber');
	$('input#expiration').payment('formatCardExpiry');
	$('input#cvc').payment('formatCardCVC');


	// Stripe Code
	var stripeResponseHandler = function(status, response) {
      var $form = $('#payment-form');
 
      if (response.error) {
        // Show the errors on the form
        console.log(response.error.message);
        $form.find('.payment-errors').text(response.error.message);
        $form.find('button').prop('disabled', false);
      } else {
        // token contains id, last4, and card type
        var token = response.id;        
        // Insert the token into the form so it gets submitted to the server
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
        // and re-submit
        	// $form.get(0).submit();  

        // Sending data via ajax instead of resubmit so we don't have to reload page
        $.ajax({
        	url: '/checkout', 
        	type: 'POST', 
        	data: $form.serialize(),
			// Receive response from server and do stuff
			success: function(response) {				
				$('#payment-form').replaceWith("<h1>Thanks for ordering!</h1><p>Your order is being prepared and will be sent out shortly. Feel free to email us at <a href='mailto:threeleaftea.com@gmail.com'>threeleaftea.com@gmail.com</a> with any changes to your order."); 
			}        	
        });


      }
    };

	$('#payment-form').submit(function(event) {
	    var $form = $(this);

	    var expMonth = $('input#expiration').payment('cardExpiryVal').month;
	    var expYear = $('input#expiration').payment('cardExpiryVal').year;

	    $form.append($('<input type="hidden" data-stripe="exp_month" />').val(expMonth));
	    $form.append($('<input type="hidden" data-stripe="exp_year" />').val(expYear));


	    // Disable the submit button to prevent repeated clicks
	    $form.find('button').prop('disabled', true);

	    Stripe.card.createToken($form, stripeResponseHandler);

	    // Prevent the form from submitting with the default action
	    return false;
  	});	

  	// Prevent form submission

})
