// API Keys

// Stripe - Secret api_key
var api_key = 'sk_live_JJh8N2DEddgOHEdQZpA73b7P';
// var api_key = 'sk_test_6fX0SkATu9C5VCBqALs9UysB';
var stripe = require('stripe')(api_key);

// Sendgrid
var sendgrid  = require('sendgrid')('richardjlo', 'WKxI3Um3iH');



exports.index = function(req, res){
  res.render('index', { title: 'Three Leaf Tea' });
};

exports.checkout = function(req, res) {
	res.render('checkout', { title: 'Checkout' });
}

exports.payment = function(req, res) {
	var invoicePrice;		// Price to put in email body
	var fullAddress = req.body.address + " " + req.body.city + " " + req.body.state + " " + req.body.zip;
	var description = req.body.name + ":: " + fullAddress;


	// Set account balance chosen customer package	
	var accountBalance;
	if (req.body.optionsRadios == 0) {
		accountBalance = 4000; // Price of initial set
		invoicePrice = "$69.00";
	} else {
		accountBalance = 0; // Tea Only
		invoicePrice = "$29.00";
	};

	//Create a Customer   
	stripe.customers.create(
		{
			card : req.body.stripeToken,	 
			email : req.body.email,
			description : description,
			account_balance: accountBalance
		}, function(err, customer) {
			if (err) {
				console.log(err.message);
				return;
			}
				stripe.customers.update(customer.id, {
					plan: 'standard'
				}, function(err) {
					if (err) {
						console.log(err.message);
						return;		
					};
				}
				);
			}
	);

	// Send email receipt
	var currentDate = new Date();
	var dd = currentDate.getDate();
	var mm = currentDate.getMonth()+1; //January is 0!

	var yyyy = currentDate.getFullYear();
	if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} currentDate = mm+'/'+dd+'/'+yyyy;

	var payload   = {
	  to      : req.body.email, 
	  from    : 'threeleaftea.com@gmail.com',
	  subject : 'Three Leaf Tea Order Confirmation',
	  text    : 'Three Leaf Tea Invoice \n\nBilling Date: ' + currentDate +'\n\nOrder: Monthly Tea Subscription ' + invoicePrice + '\n\nThank you for subscribing to Three Leaf Tea! We have received your payment and are preparing your order for delivery.\n\nYou can update or cancel your order anytime by emailing us at threeleaftea.com@gmail.com.\n\nThanks, \n\nThree Leaf Tea'
	}

	sendgrid.send(payload, function(err, json) {
	  if (err) { console.error(err); }
	  console.log(json);
	});	

	
	// res.render('success', { title: 'Order Success'}); // Why is this not rendering?

	// Send confirmation that everything is done
	res.send(200); // 200
}






